import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Router, Scene } from 'react-native-router-flux';

import List from './scenes/List';
import TaskForm from './scenes/TaskForm';

const RouterWithRedux = connect()(Router);

export default class AppNavigator extends Component {
  render() {
    return (
      <RouterWithRedux>
        <Scene key="root">
          <Scene key="List" component={List} hideNavBar initial />
          <Scene key="TaskForm" component={TaskForm} hideNavBar />
        </Scene>
      </RouterWithRedux>
    );
  }
}
