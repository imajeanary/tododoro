import React, { Component } from 'react';
import { Form, Item, Input, Label } from 'native-base';

const defaultState = {
  title: '',
};

export default class TaskForm extends Component {
  constructor(props) {
    super(props);

    const defaultValues = props.todo ? props.todo.toJS() : defaultState;
    if (__DEV__) console.log('defaultValues', defaultValues);

    this.state = defaultValues;
  }
  onChange = (name) => (event) => {
    this.setState({ [name]: event.nativeEvent.text })
  }
  getValue() {
    return this.state;
  }
  render() {
    const { title } = this.state;

    return (
      <Form>
        <Item stackedLabel>
          <Label>Title</Label>
          <Input
            value={title}
            onChange={this.onChange('title')}
          />
        </Item>
      </Form>
    );
  }
}