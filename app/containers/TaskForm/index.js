import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Container, Content, Button, Text, View } from 'native-base';
import TaskForm from '../../components/TaskForm';
import guid from '../../utils/guid';
import { addTodo, editTodo, deleteTodo } from '../../todos/actions';

class TaskFormContainer extends Component {
  onButtonPress = () => {
    const formValues = this.form.getValue();
    const action = this.props.action;
    if (__DEV__) console.log('onButtonPress', formValues);

    switch (action) {
      case 'create': {
        const id = guid();
        const dateCreated = (new Date()).toISOString();
        const todo = { ...formValues, id, dateCreated };
        this.props.addTodo({ todo });
        this.props.onSuccess();
        break;
      }
      case 'edit': {
        const id = formValues.id;
        this.props.editTodo({ id, todo: formValues });
        break;
      }
      default: {
        break;
      }
    }
  }
  onDeletePress = () => {
    const { todo, onSuccess } = this.props;
    const id = todo.get('id');
    this.props.deleteTodo({ id });
    onSuccess();
  }
  render() {
    const { buttonText, action, todo } = this.props;
    return (
      <Container>
        <Content>

          <TaskForm
            ref={(el) => this.form = el}
            todo={todo}
          />

          {
            (action === 'edit') ? <View style={{ marginTop: 20, marginLeft: 20 }} ><Text>Created at {todo.get('dateCreated')}</Text></View> : null
          }

          <Button block style={{ margin: 15, marginTop: 50 }} onPress={this.onButtonPress}>
            <Text>{buttonText}</Text>
          </Button>

          {
            action === 'edit' ? (
              <Button block style={{ margin: 15, marginTop: 50 }} onPress={this.onDeletePress}>
                <Text>Delete</Text>
              </Button>
            ) : null
          }

        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  addTodo: ({ todo }) => dispatch(addTodo({ todo })),
  editTodo: ({ id, todo }) => dispatch(editTodo({ id, todo })),
  deleteTodo: ({ id }) => dispatch(deleteTodo({ id })),
});

export default connect(null, mapDispatchToProps)(TaskFormContainer);
