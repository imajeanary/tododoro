import React, { PureComponent } from 'react';
import { ListItem, Text } from 'native-base';

export default class TodoListItem extends PureComponent {
  render() {
    const { todo, onPress } = this.props;
    return (
      <ListItem onPress={onPress} key={todo.get('id')}>
        <Text>{todo.get('title')}</Text>
      </ListItem>
    );
  }
}
