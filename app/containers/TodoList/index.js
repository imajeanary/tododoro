import React, { Component } from 'react';
import { connect } from 'react-redux';
import { List } from 'native-base';
import TodoListItem from './TodoListItem';
import getTodos from '../../todos/selectors';

class TodoList extends Component {
  onTodoPress = (todo) => (ev) => {
    if (__DEV__) console.log('onTodoPress', todo, ev);
    this.props.onTodoPress(todo);
  }
  render() {
    const { todos } = this.props;
    return (
      <List>
        {
          todos.valueSeq().map((todo) => (
            <TodoListItem
              key={todo.get('id')}
              todo={todo}
              onPress={this.onTodoPress(todo)}
            />
          ))
        }
      </List>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    todos: getTodos(state),
  }
}

export default connect(mapStateToProps)(TodoList);
