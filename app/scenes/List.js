import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { Container, Content, Header, Body, Title, Fab, Icon, Button, View } from 'native-base';
import TodoList from '../containers/TodoList';

export default class List extends Component {
  onCreateTaskPress = () => {
    if (__DEV__) console.log('onCreateTaskPress');
    Actions.TaskForm({ title: 'Create Task', buttonText: 'Create', action: 'create' });
  }
  onTodoPress = (todo) => {
    if (__DEV__) console.log('onTodoPress', todo);
    Actions.TaskForm({ title: 'Task', buttonText: 'Update', action: 'edit', todo });
  }
  render() {
    return (
      <Container>

        <Header>
          <Body>
            <Title>
              Todo List
            </Title>
          </Body>
        </Header>

        <View style={{ flex: 1 }}>
          <Content>
            <TodoList onTodoPress={this.onTodoPress} />
          </Content>

          <Fab
            style={{ backgroundColor: '#5067FF' }}
            onPress={this.onCreateTaskPress}
          >
            <Icon name="add" />
          </Fab>
        </View>

      </Container>
    );
  }
}
