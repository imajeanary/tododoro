import React, { Component } from 'react';
import { Container, Content, Header, Body, Title, Fab, Icon, Button, View } from 'native-base';
import { Actions } from 'react-native-router-flux';
import TaskForm from '../containers/TaskForm';

export default class TaskFormScene extends Component {
  onSuccess() {
    Actions.pop();
  }
  render() {
    const { title, buttonText, action, todo } = this.props;
    return (
      <Container>
        <Header>
          <Body>
            <Title>
              {title}
            </Title>
          </Body>
        </Header>

        <TaskForm
          buttonText={buttonText}
          action={action}
          onSuccess={this.onSuccess}
          todo={todo}
        />
      </Container>
    );
  }
}
