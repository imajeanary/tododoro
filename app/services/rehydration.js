import { AsyncStorage } from 'react-native';
import { persistStore } from 'redux-persist-immutable';
import ReduxPersistConfig from '../config/reduxPersist';

import * as StartupActions from '../store/actions';

const updateReducers = (store: Object) => {
  const reducerVersion = ReduxPersistConfig.reducerVersion;
  const config = ReduxPersistConfig.storeConfig;
  const startup = () => {
    store.dispatch(StartupActions.startup());
  };

  // Check to ensure latest reducer version
  AsyncStorage.getItem('reducerVersion').then((localVersion) => {
    console.time(persistTimeBeforeDispatch);
    console.time(persistTimeAfterDispatch);
    if (localVersion !== reducerVersion) {
      console.log({
        name: 'PURGE',
        value: {
          'Old Version:': localVersion,
          'New Version:': reducerVersion,
        },
        preview: 'Reducer Version Change Detected',
        important: true,
      });
      // Purge store
      persistStore(store, config, startup).purge();
      AsyncStorage.setItem('reducerVersion', reducerVersion);
    } else {
      persistStore(store, config, startup);
    }
  }).catch(() => {
    persistStore(store, config, startup);
    AsyncStorage.setItem('reducerVersion', reducerVersion);
  });
};

export default { updateReducers };
