const START_UP = 'app/store/START_UP';

export const startup = (...args) => ({
  type: START_UP,
  ...args,
});
