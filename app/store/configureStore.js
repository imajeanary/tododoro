import { fromJS } from 'immutable';
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { autoRehydrate } from 'redux-persist-immutable';
import ReduxPersistConfig from '../config/reduxPersist';
import RehydrationServices from '../services/rehydration';

/**
 * configureStore
 * @param  {Object} rootReducer [description]
 * @param  {Array} sagas       [description]
 * @return {Object}             [description]
 */
export default (rootReducer, sagas) => {
  /* ------------- Redux Configuration ------------- */
  const initialState = fromJS({});
  const middleware = [];
  const enhancers = [];

  /* ------------- Saga Middleware ------------- */
  const sagaMonitor = null;
  const sagaMiddleware = createSagaMiddleware({ sagaMonitor });
  middleware.push(sagaMiddleware);

  /* ------------- Assemble Middleware ------------- */
  enhancers.push(applyMiddleware(...middleware));

  /* ------------- AutoRehydrate Enhancer ------------- */
  // add the autoRehydrate enhancer
  if (ReduxPersistConfig.active) {
    enhancers.push(autoRehydrate());
  }

  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const createAppropriateStore = createStore;
  const store = createAppropriateStore(
    rootReducer,
    initialState,
    composeEnhancers(...enhancers),
  );
  store.runSaga = sagaMiddleware.run;

  // configure persistStore and check reducer version number
  if (ReduxPersistConfig.active) {
    RehydrationServices.updateReducers(store);
  }

  // kick off root saga
  sagas.forEach((saga) => {
    sagaMiddleware.run(saga);
  });

  return store;
};
