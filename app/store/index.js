import { combineReducers } from 'redux-immutable';
import configureStore from './configureStore';
import reducers from './reducers';
import sagas from './sagas';

export default () => {
  const rootReducer = combineReducers(reducers);
  return configureStore(rootReducer, sagas);
}
