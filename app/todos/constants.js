export const ADD_TODO = 'app/todos/ADD_TODO';
export const EDIT_TODO = 'app/todos/EDIT_TODO';
export const DELETE_TODO = 'app/todos/DELETE_TODO';
