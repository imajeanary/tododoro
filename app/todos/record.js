import { Record, Map } from 'immutable';

const TodoRecord = Record({
  id: '',
  type: '', // Task / Reminder / Note
  title: '',
  repeatType: '', // daily, weekly, etc
  priority: '',
  description: '',
  dateCreated: null,
  numberOfPomodoros: 0,
  timesCompleted: 0,
  timesCancelled: 0,
  startDate: null,
  dateCompleted: null,
  completed: false,
  cancelled: false,
  pomodoro: new Map(),
});

export default TodoRecord;
