import { Map } from 'immutable';
import TodoRecord from './record';
import {
  ADD_TODO,
  EDIT_TODO,
  DELETE_TODO,
} from './constants';

const initialState = new Map();

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO: {
      const { todo } = action;
      const id = todo.id;
      return state.set(id, new TodoRecord(todo));
    }
    case EDIT_TODO: {
      const { id, todo } = action;
      return state.mergeIn([id], todo);
    }
    case DELETE_TODO: {
      const { id } = action;
      return state.delete(id);
    }
    default: {
      return state;
    }
  }
}
